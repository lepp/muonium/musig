cmake_minimum_required(VERSION 3.13)
project(musig)

set(CMAKE_CXX_STANDARD 17)

find_package(Geant4 REQUIRED ui_all vis_all)

include(${Geant4_USE_FILE})
include_directories(${PROJECT_SOURCE_DIR}/include)

file(GLOB sources CONFIGURE_DEPENDS ${PROJECT_SOURCE_DIR}/src/*.cpp)
list(APPEND sources ${PROJECT_SOURCE_DIR}/src/G4LindhardPartition.cc)
list(APPEND sources ${PROJECT_SOURCE_DIR}/src/G4ScreenedNuclearRecoil.cc)

file(GLOB headers CONFIGURE_DEPENDS ${PROJECT_SOURCE_DIR}/include/*.h)
list(APPEND headers ${PROJECT_SOURCE_DIR}/include/G4LindhardPartition.hh)
list(APPEND headers ${PROJECT_SOURCE_DIR}/include/G4ScreenedNuclearRecoil.hh)
list(APPEND headers ${PROJECT_SOURCE_DIR}/include/c2_factory.hh)
list(APPEND headers ${PROJECT_SOURCE_DIR}/include/c2_function.hh)
list(APPEND headers ${PROJECT_SOURCE_DIR}/include/c2_function.icc)

add_executable(musig main.cpp ${sources} ${headers})
target_link_libraries(musig ${Geant4_LIBRARIES})
if (${APPLE})
    target_link_libraries(musig argp)
endif ()
