#!/bin/bash


# Exit on error
set -eEuo pipefail


if (( ${#} < 3 )); then
  echo "Usage: $(basename "${BASH_SOURCE[0]}") MUSIGEXE OUTPUTDIR MACROS..."
  exit 1
fi


run() {
  musig_exe="${1}"
  output_dir="${2}"
  macro="${3}"
  output_file="${output_dir}/$(basename "${macro}" ".mac")"
  "${musig_exe}" -o "${output_file}" "${macro}" > "${output_file}.log" 2>&1
  ret=${?}
  if (( ${ret} )); then
    echo "FAILURE"
  else
    echo "SUCCESS"
  fi
  return ${ret}
}
export -f run


musig_exe="${1}"
shift
output_dir="${1}"
shift
parallel --bar --line-buffer --tag run "${musig_exe}" "${output_dir}" ::: "${@}" && ret=0 || ret=${?}
echo
if (( ${ret} < 0 || ${ret} > 101 )); then
  echo "ERROR: GNU parallel failed with status ${ret}!"
elif (( ${ret} == 101 )); then
  echo "WARNING: More than 100 jobs failed!"
elif (( ${ret} )); then
  echo "WARNING: ${ret} jobs failed!"
else
  echo "All jobs succeeded!"
fi
