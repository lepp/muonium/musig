#include <string>

#include <argp.h>

#include <G4VSteppingVerbose.hh>
#include <G4RunManager.hh>
#include <G4UImanager.hh>
#include <G4VisExecutive.hh>
#include <G4UIterminal.hh>
#include <G4UIExecutive.hh>

#include "musigSteppingVerbose.h"
#include "musigDetectorConstruction.h"
#include "musigPhysicsList.h"
#include "musigActionInitialization.h"
#include "musigRunAction.h"


const char *argp_program_bug_address = "<dg.code+musig@protonmail.ch>";

// Program documentation
static char doc[] = "MuSiG -- Muonium Simulation using Geant4";

// Description of positional arguments
static char args_doc[] = "MACRO...";

// Optional arguments
static struct argp_option options[] = {{"seed",     's',    "SEED",     0,  "Set PRNG seed",    0},
                                       {"nevents",  'n',    "NEVENTS",  0,  "Number of events", 0},
                                       {"outfile",  'o',    "OUTFILE",  0,  "Output filename",  0},
                                       {"graphics", 'g',    nullptr,    0,  "Enable graphics",  0},
                                       {nullptr,    0,      nullptr,    0,  nullptr,            0}};

// Used by main to communicate with parse_opt
typedef struct arguments {
    unsigned long long seed = 0;
    unsigned long long nEvents = 0;
    std::string outfile;
    bool graphics = false;
    std::vector<std::string> macros;
} arguments;

// Argp parser function
static error_t parse_opt(int key, char *arg, struct argp_state *state) {
    // Get the input argument from argp_parse, which we know is a pointer to our arguments structure
    auto args = static_cast<arguments *>(state->input);
    switch (key) {
        case 's':
            args->seed = std::stoull(arg);
            break;
        case 'n':
            args->nEvents = std::stoull(arg);
            break;
        case 'o':
            args->outfile = arg;
            break;
        case 'g':
            args->graphics = true;
            break;
        case ARGP_KEY_NO_ARGS:
            argp_usage(state);
            break;
        case ARGP_KEY_ARGS:
            for (int idx = state->next; idx < state->argc; ++idx) {
                args->macros.push_back(state->argv[idx]);
            }
            break;
        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

// Argp parser
static struct argp parser = {options, parse_opt, args_doc, doc, nullptr, nullptr, nullptr};


std::string removeExtension(const std::string &filename, const std::string &extension) {
    const std::size_t fnLen = filename.length();
    const std::size_t extLen = extension.length();
    if ((fnLen > extLen) && (filename.substr((fnLen - extLen), extLen) == extension)) {
        return filename.substr(0, (fnLen - extLen));
    }
    return filename;
}


int main(int argc, char *argv[]) {
    arguments args;
    argp_parse(&parser, argc, argv, 0, nullptr, &args);

    // We handle all arguments ourselves, so only pass on the program name
    G4UIExecutive *uiExecutive = (args.graphics ? (new G4UIExecutive(1, argv)) : nullptr);

    G4VSteppingVerbose::SetInstance(new MuSiG::SteppingVerbose());

    auto runManager = new G4RunManager();
    runManager->SetUserInitialization(new MuSiG::DetectorConstruction());
    runManager->SetUserInitialization(new MuSiG::PhysicsList());
    runManager->SetUserInitialization(new MuSiG::ActionInitialization());

    auto uiManager = G4UImanager::GetUIpointer();

    auto visManager = new G4VisExecutive();
    visManager->Initialize();

    if (!args.outfile.empty()) {
        // There is a messenger for this (/run/outputFilename), but it only works after /run/initialize, which happens in the macro
        MuSiG::RunAction::OutputFilename() = removeExtension(args.outfile, ".root");
    }
    if (args.seed) {
        uiManager->ApplyCommand("/gun/seed " + std::to_string(args.seed));
    }
    for (const auto &macro : args.macros) {
        uiManager->ApplyCommand("/control/execute " + macro);
    }
    if (args.nEvents) {
        uiManager->ApplyCommand("/run/beamOn " + std::to_string(args.nEvents));
    }
    if (args.graphics) {
        uiExecutive->SessionStart();
        delete uiExecutive;
    }

    // Currently causes a seg fault...
    //delete visManager;
    //delete runManager;

    return 0;
}
