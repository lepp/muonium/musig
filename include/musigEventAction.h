#ifndef MUSIG_EVENTACTION_H
#define MUSIG_EVENTACTION_H


#include <G4UserEventAction.hh>
#include <G4String.hh>
#include <G4Types.hh>

#include <map>
#include <string>


namespace MuSiG {


    class EventAction : public G4UserEventAction {
    public:
        EventAction();

        ~EventAction() override;

        void BeginOfEventAction(const G4Event *) override;

        void EndOfEventAction(const G4Event *) override;
    };


}


#endif