#ifndef MUSIG_TRACKERSD_H
#define MUSIG_TRACKERSD_H

#include <G4VSensitiveDetector.hh>

#include "musigTrackerHit.h"


namespace MuSiG {


    class TrackerSD : public G4VSensitiveDetector {
    public:
        explicit TrackerSD(const G4String &);

        ~TrackerSD() override;

        void Initialize(G4HCofThisEvent *) override;

        G4bool ProcessHits(G4Step *, G4TouchableHistory *) override;

        void EndOfEvent(G4HCofThisEvent *) override;

    private:
        TrackerHitsCollection *trackerCollection = nullptr;
    };


}

#endif

