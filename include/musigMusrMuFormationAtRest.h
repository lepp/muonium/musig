// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
//  Muonium Formation at Rest in materials of interest
//  Id    : musrMuFormationAtRest.hh, v 1.0
//  Author: Anna Soter
//  Date  : 2018
// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


#ifndef MUSIG_MUSRMUFORMATIONATREST_H
#define MUSIG_MUSRMUFORMATIONATREST_H


#include <G4VRestProcess.hh>
#include <G4ParticleTable.hh>


namespace MuSiG {


    class MusrMuFormationAtRest : public G4VRestProcess {
    public:

        explicit MusrMuFormationAtRest(const G4String &name = "MuFormationAtRest");

        using G4VRestProcess::IsApplicable; // Avoid overloaded virtual warning

        static G4bool IsApplicable(const G4ParticleDefinition &, const G4Track &);

        void PreparePhysicsTable(const G4ParticleDefinition &) override;

        // null physics table
        void BuildPhysicsTable(const G4ParticleDefinition &) override;


        ~MusrMuFormationAtRest() override;

        //! - Main method. Muonium formation process is executed at the END of a step. */
        G4VParticleChange *AtRestDoIt(const G4Track &, const G4Step &) override;

        // zero mean lifetime
        G4double GetMeanLifeTime(const G4Track &, G4ForceCondition *) override {
            return 0.;
        }

        G4String p_name;


        void SetMuonium(const G4Step *aStep);

        void SetMuon(const G4Step *aStep);

        G4double AtRestGetPhysicalInteractionLength(const G4Track &, G4ForceCondition *) override;


        // model parameters
        G4ParticleTable *particleTable = nullptr;
        G4ParticleDefinition *particle = nullptr;
        G4double rnd = 0.;
        G4DynamicParticle *DP = nullptr;

        //! The particle change object.
        G4VParticleChange fParticleChange;

        void PrepareSecondary(const G4Track &);

        G4Track *aSecondary = nullptr;
    };


}


#endif
