// ------------------------------------------------------------
//      GEANT 4 class header file
//
//      History:
//               17 August 2004 P.Gumplinger and T.MacPhail
//               samples Michel spectrum including 1st order
//               radiative corrections
//               Reference: Florian Scheck "Muon Physics", in Physics Reports
//                          (Review Section of Physics Letters) 44, No. 4 (1978)
//                          187-248. North-Holland Publishing Company, Amsterdam
//                          at page 210 cc.
//
//                          W.E. Fisher and F. Scheck, Nucl. Phys. B83 (1974) 25.
//
// ------------------------------------------------------------
#ifndef MUSIG_MUDECAYCHANNELWITHSPIN_H
#define MUSIG_MUDECAYCHANNELWITHSPIN_H

#include <globals.hh>
#include <G4ThreeVector.hh>

#include "musigMuDecayChannel.h"


namespace MuSiG {


    class MuDecayChannelWithSpin : public MuDecayChannel {
        // Class Decription
        // This class describes muon decay kinemtics.
        // This version assumes V-A coupling with 1st order radiative correctons,
        //              the standard model Michel parameter values, but
        //              gives incorrect energy spectrum for neutrinos

    public:  // With Description

        //Constructors
        MuDecayChannelWithSpin(const G4String &theParentName, G4double theBR);

        //  Destructor
        ~MuDecayChannelWithSpin() override;

    public:  // With Description

        G4DecayProducts *DecayIt(G4double) override;


    private:

// Radiative Correction Factors

        G4double F_c(G4double x, G4double x0);

        G4double F_theta(G4double x, G4double x0);

        [[nodiscard]] G4double R_c(G4double x) const;

        G4double EMMU = 0.;
        G4double EMASS = 0.;

    };


}

#endif
