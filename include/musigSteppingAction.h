#ifndef MUSIG_STEPPINGACTION_H
#define MUSIG_STEPPINGACTION_H

#include <G4UserSteppingAction.hh>


namespace MuSiG {


    class SteppingAction : public G4UserSteppingAction {
    public:
        SteppingAction();

        ~SteppingAction() override;

        void UserSteppingAction(const G4Step *) override;
    };


}


#endif
