#ifndef MUSIG_RUNACTION_H
#define MUSIG_RUNACTION_H

#include <G4UserRunAction.hh>
#include <globals.hh>
#include <G4RootAnalysisManager.hh>
#include <G4GenericMessenger.hh>
#include <G4String.hh>


namespace MuSiG {


    class RunAction : public G4UserRunAction {
    public:
        RunAction();

        ~RunAction() override;

        void BeginOfRunAction(const G4Run *) override;

        void EndOfRunAction(const G4Run *) override;

        static G4String &OutputFilename() {
            static G4String outputFilename = "musig_out";
            return outputFilename;
        }

    private:
        G4RootAnalysisManager *fAnalMan = nullptr;

        G4GenericMessenger fMessenger;
    };


}


#endif





