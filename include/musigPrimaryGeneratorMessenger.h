#ifndef MUSIG_PRIMARYGENERATORMESSENGER_H
#define MUSIG_PRIMARYGENERATORMESSENGER_H


#include <G4UImessenger.hh>
#include <globals.hh>
#include <G4UIcmdWithALongInt.hh>
#include <G4UIcmdWithAString.hh>
#include <G4UIcmdWith3VectorAndUnit.hh>
#include <G4UIcmdWithADoubleAndUnit.hh>
#include <G4UIcmdWith3Vector.hh>
#include <G4UIcmdWithADouble.hh>

#include "musigPrimaryGeneratorAction.h"


namespace MuSiG {


    class PrimaryGeneratorAction;


    class PrimaryGeneratorMessenger : public G4UImessenger {
    public:
        explicit PrimaryGeneratorMessenger(PrimaryGeneratorAction *);

        ~PrimaryGeneratorMessenger() override;

        void SetNewValue(G4UIcommand *, G4String) override;

    private:
        PrimaryGeneratorAction *fAction = nullptr;
        G4UIdirectory *setGunDir = nullptr;

        G4UIcmdWithALongInt *setSeedCmd = nullptr;
        G4UIcmdWithAString *setPrimaryParticleCmd = nullptr;
        G4UIcmdWith3VectorAndUnit *setVertexCmd = nullptr;
        G4UIcmdWith3VectorAndUnit *setVertexSigmaCmd = nullptr;
        G4UIcmdWithADoubleAndUnit *setVertexRelativeRCmd = nullptr;
        G4UIcmdWithADoubleAndUnit *setStarttimeCmd = nullptr;
        G4UIcmdWithADoubleAndUnit *setStarttimeSigmaCmd = nullptr;
        G4UIcmdWithADoubleAndUnit *setKEnergyCmd = nullptr;
        G4UIcmdWithADoubleAndUnit *setMomentumCmd = nullptr;
        G4UIcmdWithADoubleAndUnit *setMomentumSigmaCmd = nullptr;
        G4UIcmdWith3VectorAndUnit *setTiltAngleCmd = nullptr;
        G4UIcmdWith3VectorAndUnit *setSigmaTiltAngleCmd = nullptr;
        G4UIcmdWithADoubleAndUnit *setPitchCmd = nullptr;
        G4UIcmdWith3Vector *setMuonPolarizCmd = nullptr;
        G4UIcmdWith3Vector *setDirectionCmd = nullptr;
        G4UIcmdWithADouble *setMuonPolarizFractionCmd = nullptr;
    };


}


#endif