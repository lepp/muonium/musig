#ifndef MUSIG_TRACKERHIT_H
#define MUSIG_TRACKERHIT_H


#include <G4VHit.hh>
#include <G4THitsCollection.hh>
#include <G4Allocator.hh>
#include <G4ThreeVector.hh>
#include <G4RotationMatrix.hh>
#include <G4LogicalVolume.hh>


namespace MuSiG {
    class TrackerHit : public G4VHit {
    public:

        TrackerHit();

        ~TrackerHit() override;

        TrackerHit(const TrackerHit &);

        TrackerHit &operator=(const TrackerHit &);

        G4int operator==(const TrackerHit &) const;

        void *operator new(size_t);

        void operator delete(void *);

        void Draw() override;

        void Print() override;

        void Add(G4double de);

        void SetEventNb(const G4int e_nr) {
            eventNb = e_nr;
        };

        void SetTrackID(const G4int track) {
            trackID = track;
        };

        void SetDetectorName(const G4String &name) {
            detectorName = name;
        };

        void SetChamberNb(const G4int chamb) {
            chamberNb = chamb;
        };

        void SetEdep(const G4double de) {
            edep = de;
        };

        void SetPos(const G4ThreeVector &xyz) {
            pos = xyz;
        };

        void SetTime(const G4double tim) {
            time = tim;
        };

        void SetParticleName(const G4String &pname) {
            particleName = pname;
        };

        void SetParticleVertex(const G4ThreeVector &pvertex) {
            particleVertex = pvertex;
        };

        [[nodiscard]] G4int GetEventNb() const {
            return eventNb;
        };

        [[nodiscard]] G4int GetTrackID() const {
            return trackID;
        };

        [[nodiscard]] const G4String &GetDetectorName() const {
            return detectorName;
        };

        [[nodiscard]] G4int GetChamberNb() const {
            return chamberNb;
        };

        [[nodiscard]] G4double GetEdep() const {
            return edep;
        };

        [[nodiscard]] const G4ThreeVector &GetPos() const {
            return pos;
        };

        [[nodiscard]] G4double GetTime() const {
            return time;
        };

        [[nodiscard]] const G4String &GetParticleName() const {
            return particleName;
        };

        [[nodiscard]] const G4ThreeVector &GetParticleVertex() const {
            return particleVertex;
        };

        static G4Allocator<TrackerHit> &TrackerHitAllocator() {
            static G4Allocator<TrackerHit> trackerHitAllocator;
            return trackerHitAllocator;
        }

    private:
        G4int eventNb = 0;
        G4int trackID = 0;
        G4String detectorName;
        G4int chamberNb = 0;
        G4double edep = 0.;
        G4ThreeVector pos;
        G4double time = 0.;
        G4String particleName;
        G4ThreeVector particleVertex;
    };


    typedef G4THitsCollection<TrackerHit> TrackerHitsCollection;
}


#endif
