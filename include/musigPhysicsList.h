#ifndef MUSIG_PHYSICSLIST_H
#define MUSIG_PHYSICSLIST_H


#include <G4VModularPhysicsList.hh>
#include <globals.hh>

#include "musigMuDecayChannel.h"
#include "musigMuDecayChannelWithSpin.h"
#include "musigStepMax.h"
#include "musigPhysicsListMessenger.h"


namespace MuSiG {


    class PhysicsListMessenger;


    class PhysicsList : public G4VModularPhysicsList {
    public:

        PhysicsList();

        ~PhysicsList() override;

        void ConstructParticle() override;

        void AddPhysicsList(const G4String &name);

        void ConstructProcess() override;

        void AddStepMax();

        StepMax *GetStepMaxProcess() {
            return fStepMaxProcess;
        }

    private:

        void AddIonGasModels();

        G4bool fHelIsRegisted = false;
        G4bool fBicIsRegisted = false;
        G4bool fBiciIsRegisted = false;

        G4String fEmName;
        G4VPhysicsConstructor *fEmPhysicsList = nullptr;
        G4VPhysicsConstructor *fDecPhysicsList = nullptr;
        std::vector<G4VPhysicsConstructor *> fHadronPhys;
        StepMax *fStepMaxProcess = nullptr;

        PhysicsListMessenger *fMessenger = nullptr;
    };


}


#endif

