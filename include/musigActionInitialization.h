//
// Created by damian on 09.01.22.
//

#ifndef MUSIG_ACTIONINITIALIZATION_H
#define MUSIG_ACTIONINITIALIZATION_H


#include <G4VUserActionInitialization.hh>


namespace MuSiG {


    class ActionInitialization : public G4VUserActionInitialization {
    public:
        ActionInitialization();

        ~ActionInitialization() override;

        void Build() const override;
    };


}


#endif //MUSIG_ACTIONINITIALIZATION_H
