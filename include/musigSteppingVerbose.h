#ifndef MUSIG_STEPPINGVERBOSE_H
#define MUSIG_STEPPINGVERBOSE_H


#include <G4SteppingVerbose.hh>


namespace MuSiG {


    class SteppingVerbose : public G4SteppingVerbose {
    public:

        SteppingVerbose();

        ~SteppingVerbose() override;

        void StepInfo() override;

        void TrackingStarted() override;

    };


}


#endif
