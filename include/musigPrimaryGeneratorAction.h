#ifndef MUSIG_PRIMARYGENERATORACTION_H
#define MUSIG_PRIMARYGENERATORACTION_H


#include <G4VUserPrimaryGeneratorAction.hh>
#include <globals.hh>
#include <Randomize.hh>
#include <G4ThreeVector.hh>
#include <G4ParticleDefinition.hh>
#include <G4ParticleGun.hh>
#include <G4GeneralParticleSource.hh>
#include <G4SystemOfUnits.hh>


namespace MuSiG {


    class PrimaryGeneratorMessenger;


    class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction {

    public:
        PrimaryGeneratorAction();

        ~PrimaryGeneratorAction() override;

    public:

        void GeneratePrimaries(G4Event *) override;

        G4ParticleGun *GetParticleGun() {
            return particleGun;
        };

        void SetPrimaryParticle(const G4String &particleName);

        void SetSeed(G4long val) {
            seed = val;
        }

        void SetVertex(const G4ThreeVector &v) {
            x0 = v[0];
            y0 = v[1];
            z0 = v[2];
        }

        void SetVertexSigma(const G4ThreeVector &v) {
            xSigma = v[0];
            ySigma = v[1];
            zSigma = v[2];
        }

        void SetVertexRelativeR(G4double val) {
            relativeRMaxAllowed = val;
        }

        void SetStartTime(G4double val) {
            t0 = val;
        }

        void SetStartTimeSigma(G4double val) {
            tSigma = val;
        }

        void SetKEnergy(G4double val);

        void SetMomentum(G4double val) {
            p0 = val;
        }

        void SetMomentumSigma(G4double val) {
            pSigma = val;
        }

        void SetTilt(const G4ThreeVector &v) {
            xangle0 = v[0];
            yangle0 = v[1];
        }

        void SetSigmaTilt(const G4ThreeVector &v) {
            xangleSigma = v[0];
            yangleSigma = v[1];
            zangleSigma = v[2];
        }

        void SetPitch(G4double val) {
            pitch = val;
        }

        void SetBeamDirection(const G4ThreeVector &vIniDir);

        void SetInitialMuonPolariz(const G4ThreeVector &vIniPol);

        void SetOrReadTheRandomNumberSeeds(G4Event *anEvent) const;


        void SetInitialPolarizFraction(G4double val) {
            if ((val > 1.) || (val < -1.)) {
                G4cout << "SetInitialPolarizFraction(): polarisation fraction out of range (" << val << ")" << G4endl;
                exit(1);
            }
            polarisFraction = val;
        }


    private:

        G4ParticleGun *particleGun = nullptr;

        PrimaryGeneratorMessenger *gunMessenger = nullptr;

        G4long seed = 1;

        G4double x0 = 0.;
        G4double y0 = 0.;
        G4double z0 = -10. * cm;
        G4double xSigma = 0.;
        G4double ySigma = 0.;
        G4double zSigma = 0.;
        G4double t0 = 0.;
        G4double tSigma = 0.;
        G4double relativeRMaxAllowed = 1e10 * mm;
        G4double p0 = 0.;
        G4double pSigma = 0.;
        G4double xangle0 = 0.;
        G4double yangle0 = 0.;
        G4double xangleSigma = 0.;
        G4double yangleSigma = 0.;
        G4double zangleSigma = 0.;
        G4double pitch = 0.;

        G4bool UnpolarisedMuonBeam = false;
        G4bool TransversalyUnpolarisedMuonBeam = false;

        G4double xPolarisIni = 1.;
        G4double yPolarisIni = 0.;
        G4double zPolarisIni = 0.;
        G4double xDirection = 0.;
        G4double yDirection = 0.;
        G4double zDirection = 1.;
        G4double polarisFraction = 1.;
    };


}


#endif


