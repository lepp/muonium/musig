# MuSiG

Muonium Simulation using Geant4

## Requirements

MuSiG should compile on GNU/Linux and macOS. The following packages are required:

- Geant4
- ROOT
- GNU argp

A clean and easy way to install these is to install [Miniconda](https://docs.conda.io/en/latest/miniconda.html)
(or [Anaconda](https://anaconda.com)) and then create a new environment with the following commands (switching the
default channel to [conda-forge](https://conda-forge.org)):

```
$ conda create -n musig
$ conda activate musig
$ conda config --add channels conda-forge
$ conda config --set channel_priority strict
$ conda install root geant4
```

If you're on macOS, you also need to install argp:

```
$ conda install argp-standalone
```

Replace `musig` with a name of your liking.

In addition, a working C++ toolchain is required. On Debian-based GNU/Linux distributions this can usually be achieved
by installing `build-essential`:

```
# apt install build-essential
```

## Installation

1. Clone this repository:

```
$ git clone https://gitlab.ethz.ch/lepp/muonium/simon.git
```

2. Source the required environment. If you've followed the steps in [Requirements](#requirements):

```
$ conda activate musig
```

3. Build MuSiG:

```
$ cd /path/to/musig
$ mkdir build
$ cd build
$ cmake ..
$ cmake --build . -- -j $(nproc)
```

## Usage

To get a quick overview of the available options run:

```
musig --help
```

### Notes

- A macro is always required.
- Currently, also a seed is required as the default value (0) will lead to a crash.
