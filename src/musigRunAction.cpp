#include "musigRunAction.h"

#include <G4Run.hh>
#include <G4RootAnalysisManager.hh>


namespace MuSiG {


    RunAction::RunAction() : fMessenger(this, "/run/") {
        fMessenger.DeclareProperty("outputFilename", OutputFilename(), "Output filename without extension");

        fAnalMan = G4RootAnalysisManager::Instance();

        auto id = fAnalMan->CreateNtuple("t1", "Tree with data from pion capture at PSI");
        fAnalMan->CreateNtupleIColumn(id, "eventNb");
        fAnalMan->CreateNtupleSColumn(id, "partname");
        fAnalMan->CreateNtupleIColumn(id, "trackID");
        fAnalMan->CreateNtupleSColumn(id, "detname");
        fAnalMan->CreateNtupleIColumn(id, "chamberNb");
        fAnalMan->CreateNtupleFColumn(id, "edeposit");
        fAnalMan->CreateNtupleFColumn(id, "x");
        fAnalMan->CreateNtupleFColumn(id, "y");
        fAnalMan->CreateNtupleFColumn(id, "z");
        fAnalMan->CreateNtupleFColumn(id, "time");
        fAnalMan->CreateNtupleFColumn(id, "x_vert");
        fAnalMan->CreateNtupleFColumn(id, "y_vert");
        fAnalMan->CreateNtupleFColumn(id, "z_vert");
        fAnalMan->FinishNtuple(id);

        id = fAnalMan->CreateNtuple("t2", "Tree with integrated data from pion capture at PSI");
        fAnalMan->CreateNtupleFColumn(id, "Iedeposit");
        fAnalMan->CreateNtupleFColumn(id, "Itime");
        fAnalMan->CreateNtupleIColumn(id, "IEventNb");
        fAnalMan->CreateNtupleIColumn(id, "IchamberNb");
        fAnalMan->CreateNtupleSColumn(id, "Idetname");
        fAnalMan->FinishNtuple(id);

        id = fAnalMan->CreateNtuple("t3", "Tree for analysis");
        fAnalMan->CreateNtupleDColumn(id, "testTime");
        fAnalMan->CreateNtupleDColumn(id, "testEnergy");
        fAnalMan->CreateNtupleSColumn(id, "testName");
        fAnalMan->CreateNtupleSColumn(id, "testProc");
        fAnalMan->CreateNtupleIColumn(id, "counter1");
        fAnalMan->FinishNtuple(id);

        id = fAnalMan->CreateNtuple("t4", "stopping distribution");
        fAnalMan->CreateNtupleDColumn(id, "x");
        fAnalMan->CreateNtupleDColumn(id, "y");
        fAnalMan->CreateNtupleDColumn(id, "z");
        fAnalMan->CreateNtupleIColumn(id, "stopHP");
        fAnalMan->CreateNtupleIColumn(id, "stopP");
        fAnalMan->CreateNtupleIColumn(id, "stopF");
        fAnalMan->CreateNtupleIColumn(id, "stopE");
        fAnalMan->FinishNtuple(id);
    }

    RunAction::~RunAction() = default;

    void RunAction::BeginOfRunAction(const G4Run *aRun) {
        fAnalMan->OpenFile(OutputFilename() + "_" + std::to_string(aRun->GetRunID()) + ".root");

        G4cout << "############ Run " << aRun->GetRunID() << " start." << G4endl;
    }

    void RunAction::EndOfRunAction(const G4Run *) {
        fAnalMan->Write();
        fAnalMan->CloseFile();
    }


}