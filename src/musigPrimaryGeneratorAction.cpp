#include "musigPrimaryGeneratorAction.h"

#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"

#include <vector>
#include <string>
#include <cstdlib>
#include <cmath>
#include <ctgmath>

#include "musigPrimaryGeneratorMessenger.h"


namespace MuSiG {


    PrimaryGeneratorAction::PrimaryGeneratorAction() : G4VUserPrimaryGeneratorAction() {
        //create a messenger for this class
        gunMessenger = new PrimaryGeneratorMessenger(this);

        // default particle kinematic
        G4ParticleTable *particleTable = G4ParticleTable::GetParticleTable();
        G4ParticleDefinition *muonParticle = particleTable->FindParticle("mu+");

        const G4int n_particle = 1;
        particleGun = new G4ParticleGun(n_particle);
        particleGun->SetParticleDefinition(muonParticle);

    }


    PrimaryGeneratorAction::~PrimaryGeneratorAction() {
        delete particleGun;
        delete gunMessenger;
    }

    void PrimaryGeneratorAction::GeneratePrimaries(G4Event *anEvent) {
        //this function is called at the beginning of event
        //randomize the beam, if requested.

        SetOrReadTheRandomNumberSeeds(anEvent);

        G4double x, y, z;
        G4double p;
        G4double xangle, yangle;
        G4double sinXangle, sinYangle;
        G4double px, py, pz;

        G4int checkNrOfCounts = 0;
        do {
            if (xSigma > 0) {
                x = G4RandGauss::shoot(x0, xSigma);
            }           //  Gaussian distribution
            else if (xSigma < 0) {
                x = x0 + xSigma * (G4UniformRand() * 2. - 1.);
            }     //  Uniform step distribution
            else {
                x = x0;
            }                                     //  Point-like

            if (ySigma > 0) {
                y = G4RandGauss::shoot(y0, ySigma);
            } else if (ySigma < 0) {
                y = y0 + ySigma * (G4UniformRand() * 2. - 1.);
            } else {
                y = y0;
            }

            if (zSigma > 0) {
                z = G4RandGauss::shoot(z0, zSigma);
            } else if (zSigma < 0) {
                z = z0 + zSigma * (G4UniformRand() * 2. - 1.);
            } else {
                z = z0;
            }
            checkNrOfCounts++;
            if (checkNrOfCounts > 1000) {
                G4cout
                        << "musrPrimaryGeneratorAction::GeneratePrimaries:  Too strict requirements on the r or z position!"
                        << G4endl;
            }
            // The generated muon has to stay within some well defined region,  e.g. within the beampipe
        } while (((x - x0) * (x - x0) + (y - y0) * (y - y0)) > (relativeRMaxAllowed * relativeRMaxAllowed));

        if (pSigma > 0) {
            p = G4RandGauss::shoot(p0, pSigma);
        } else {
            p = p0;
        }

        // Add some initial angle (px and py component of the momentum)
        if (xangleSigma > 0) {
            xangle = G4RandGauss::shoot(xangle0, xangleSigma);
        } else {
            xangle = xangle0;
        }
        //  Add the beam tilt, which depends on the distance from the beam centre.
        if (pitch != 0) {
            xangle += -pitch * (x - x0);
        }

        if (yangleSigma > 0) {
            yangle = G4RandGauss::shoot(yangle0, yangleSigma);
        } else {
            yangle = yangle0;
        }
        //  Add the beam tilt, which depends on the distance from the beam centre.
        if (pitch != 0) {
            yangle += -pitch * (y - y0);
        }

        sinXangle = sin(xangle);
        sinYangle = sin(yangle);
        px = p * sinXangle;
        py = p * sinYangle;
        pz = std::sqrt(p * p - px * px - py * py);

        G4double ParticleTime; //P.B. 13 May 2009
        if (tSigma > 0) {
            ParticleTime = G4RandGauss::shoot(t0, tSigma);
        }         //  Gaussian distribution
        else if (tSigma < 0) {
            ParticleTime = t0 + tSigma * (G4UniformRand() * 2. - 1.);
        }   //  Uniform step distribution
        else {
            ParticleTime = t0;
        }  //  Point-like

        if ((xDirection == 0) && (yDirection == 0)) {
            // Rotation does not work for beam direction along z.
            pz = zDirection * pz;
            // No change to the beam spot...
        } else {
            auto PVec = new G4ThreeVector(px, py, pz);
            PVec->rotate(std::acos(zDirection), G4ThreeVector(-yDirection, xDirection, 0));
            px = PVec->x();
            py = PVec->y();
            pz = PVec->z();

            // Rotate also beam spot
            auto RVec = new G4ThreeVector(x - x0, y - y0, z - z0);
            RVec->rotate(std::acos(zDirection), G4ThreeVector(-yDirection, xDirection, 0));
            x = x0 + RVec->x();
            y = y0 + RVec->y();
            z = z0 + RVec->z();
        }

        // Assign spin
        G4double xpolaris;
        G4double ypolaris;
        G4double zpolaris;
        if (UnpolarisedMuonBeam) {
            // for genarating random numbers on the sphere see  http://mathworld.wolfram.com/SpherePointPicking.html
            G4double thetaTMP = CLHEP::pi / 2;
            if (!TransversalyUnpolarisedMuonBeam) {
                thetaTMP = acos(2. * G4UniformRand() - 1);
            }
            G4double phiTMP = 2. * CLHEP::pi * G4UniformRand();

            xpolaris = std::sin(thetaTMP) * std::cos(phiTMP);
            ypolaris = std::sin(thetaTMP) * std::sin(phiTMP);
            zpolaris = std::cos(thetaTMP);
        } else {
            if (G4UniformRand() > ((1. - polarisFraction) / 2.)) {
                xpolaris = xPolarisIni;
                ypolaris = yPolarisIni;
                zpolaris = zPolarisIni;
            } else {
                xpolaris = -xPolarisIni;
                ypolaris = -yPolarisIni;
                zpolaris = -zPolarisIni;
            }
        }

        particleGun->SetParticlePosition(G4ThreeVector(x, y, z));
        G4double particle_mass = particleGun->GetParticleDefinition()->GetPDGMass();
        G4double particleEnergy = std::sqrt(p * p + particle_mass * particle_mass) - particle_mass;
        particleGun->SetParticleEnergy(particleEnergy);
        particleGun->SetParticleTime(ParticleTime);
        particleGun->SetParticleMomentumDirection(G4ThreeVector(px, py, pz));
        particleGun->SetParticlePolarization(G4ThreeVector(xpolaris, ypolaris, zpolaris));
        particleGun->GeneratePrimaryVertex(anEvent);


    }


    void PrimaryGeneratorAction::SetInitialMuonPolariz(const G4ThreeVector &vIniPol) {
        G4double magnitude = vIniPol.mag();
        if (magnitude < 0.00000001) {
            G4cout << "Unpolarised initial muons" << G4endl;
            UnpolarisedMuonBeam = true;
            if ((magnitude < 0.0000000085) && (magnitude > 0.0000000075)) {
                G4cout << "Transversaly unpolarised initial muons" << G4endl;
                TransversalyUnpolarisedMuonBeam = true;
            }
        } else {
            xPolarisIni = vIniPol(0) / magnitude;
            yPolarisIni = vIniPol(1) / magnitude;
            zPolarisIni = vIniPol(2) / magnitude;
            G4cout << "Initial Muon Polarisation set to (" << xPolarisIni << "," << yPolarisIni << "," << zPolarisIni
                   << ")" << G4endl;
        }
    }

//===============================================================================
    void PrimaryGeneratorAction::SetBeamDirection(const G4ThreeVector &vIniDir) {
        G4double magnitude = vIniDir.mag();
        xDirection = vIniDir(0) / magnitude;
        yDirection = vIniDir(1) / magnitude;
        zDirection = vIniDir(2) / magnitude;
        G4cout << "Initial Beam Direction set to (" << xDirection << "," << yDirection << "," << zDirection << ")"
               << G4endl;
    }

//===============================================================================

    void PrimaryGeneratorAction::SetOrReadTheRandomNumberSeeds(G4Event *anEvent) const {
        CLHEP::HepRandom::setTheSeed(seed + anEvent->GetEventID());
        CLHEP::RandGauss::setFlag(false);
    }

    void PrimaryGeneratorAction::SetKEnergy(G4double val) {

        G4double particle_mass = particleGun->GetParticleDefinition()->GetPDGMass();
        p0 = std::sqrt(val * val + 2 * particle_mass * val);
    }

//===============================================================================
    void PrimaryGeneratorAction::SetPrimaryParticle(const G4String &particleName) {
        G4ParticleTable *particleTable = G4ParticleTable::GetParticleTable();
        G4ParticleDefinition *particule = particleTable->FindParticle(particleName);
        if (particule) {
            G4cout << "   Primary particle:  USING " << particleName << " AS PRIMARY PARTICLE!" << G4endl;
            particleGun->SetParticleDefinition(particule);
        } else {
            G4cout << "\n   musrPrimaryGeneratorAction::SetPrimaryParticuleMuMinus():  Particle " << particleName
                   << " required as primary particle, but not found !!!";
            G4cout << "S T O P     F O R C E D" << G4endl;
            exit(1);
        }
    }


}


