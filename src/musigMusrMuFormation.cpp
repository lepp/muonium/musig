#include "musigMusrMuFormation.h"


namespace MuSiG {


    MusrMuFormation::MusrMuFormation(const G4String &name, G4ProcessType aType) : G4VDiscreteProcess(name, aType) {}

    MusrMuFormation::~MusrMuFormation() = default;

    G4VParticleChange *MusrMuFormation::PostStepDoIt(const G4Track &trackData,
                                                     const G4Step &aStep) { // Initialize ParticleChange  (by setting all its members equal to
        //                             the corresponding members in G4Track)
        fParticleChange.Initialize(trackData);

        G4Track theNewTrack;
        if (CheckCondition(aStep)) {
            GetDatas(&aStep);
            G4Step theStep;
            PrepareSecondary(trackData);
            fParticleChange.AddSecondary(aSecondary);
            fParticleChange.ProposeTrackStatus(fStopAndKill);
        } else {
            fParticleChange.ProposeTrackStatus(trackData.GetTrackStatus());
        }
        return &fParticleChange;
    }


    G4bool MusrMuFormation::CheckCondition(const G4Step &aStep) {
        // Decide when to call the MuFormation process
        G4bool condition = false;
        p_name = aStep.GetTrack()->GetDefinition()->GetParticleName(); // particle name
        std::string logVolName = aStep.GetTrack()->GetVolume()->GetLogicalVolume()->GetName();
        G4double eKin = aStep.GetTrack()->GetDynamicParticle()->GetKineticEnergy() / CLHEP::keV;

        if (p_name == "mu+" && logVolName == "target" && (eKin < 50.)) {
            condition = true;
        }
        return condition;
    }


    G4double MusrMuFormation::GetMeanFreePath(const G4Track &, G4double, G4ForceCondition *condition) {
        *condition = Forced;
        return DBL_MAX;
    }


    void MusrMuFormation::GetDatas(const G4Step *aStep) {    // Particle generation according to yield table
        particleTable = G4ParticleTable::GetParticleTable();
        rnd = G4UniformRand();
        G4double E = aStep->GetTrack()->GetDynamicParticle()->GetKineticEnergy() / CLHEP::keV;

        Yields::GetYields(E, 105.658369 * 1000, yvector); // Energy [keV], muon mass [keV/c2], yield table
        G4String p_new = "Mu";

        G4double p_noform = 0.00001; //probability of forming NO muonium in given volume

        // Positive muon
        if (p_name == "mu+") {
            if (rnd < p_noform) {
                particle = particleTable->FindParticle(p_name);
            } else {
                particle = particleTable->FindParticle(p_new);
            }


            double T = 270;
            double dT = 100.;

            double pi = 3.14159265358979323846;

            double ran1 = G4UniformRand();
            double ran2 = G4UniformRand();

            double sin_theta = std::sqrt(ran1);
            double cos_theta = std::sqrt(1 - sin_theta * sin_theta);

            // random in plane angle
            double psi = ran2 * 2 * pi;

            // three vector direction components
            double a = sin_theta * std::cos(psi);
            double b = sin_theta * std::sin(psi);
            double c = cos_theta;


            double e0 = (T * 8.61738569e-5);
            double eSigma = (dT * 8.61738569e-5);

            double eKin = 0.;
            while (eKin <= 0) {
                eKin = G4RandGauss::shoot(e0, eSigma);
            }

            DP = new G4DynamicParticle(particle, G4ThreeVector(a, b, -c), eKin * CLHEP::eV);


            // IMPORTANT : COPY TRACK DATA TO GET THE SAME PARTICLE PROPERTIES!!!
            // Proper time: time since creation. Decay time is preassigned from this

            DP->SetProperTime(aStep->GetTrack()->GetDynamicParticle()->GetProperTime());
            DP->SetPolarization(aStep->GetTrack()->GetDynamicParticle()->GetPolarization().x(),
                                aStep->GetTrack()->GetDynamicParticle()->GetPolarization().y(),
                                aStep->GetTrack()->GetDynamicParticle()->GetPolarization().z());
            DP->SetPreAssignedDecayProperTime(aStep->GetTrack()->GetDynamicParticle()->GetPreAssignedDecayProperTime());
        }
    }


    void MusrMuFormation::PrepareSecondary(const G4Track &track) {
        if (p_name == "mu+") {
            double dt = 0.;
            while (dt <= 0) {
                dt = G4RandGauss::shoot(500, 300);
            }
            aSecondary = new G4Track(DP, track.GetGlobalTime() + dt * CLHEP::ns, track.GetPosition());
        }
    }


}