#include "musigMusrMuFormationAtRest.h"

#include <G4HadronicProcessStore.hh>

#include <cmath>


namespace MuSiG {


    MusrMuFormationAtRest::MusrMuFormationAtRest(const G4String &name) : G4VRestProcess(name) {
        /* DG 2022/01/06:Why is this here? Blind copy-pasting? Commenting it out for now.
        G4HadronicDeprecate("MusrMuFormationAtRest");*/

        if (verboseLevel > 0) {
            G4cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " << GetProcessName() << " is created " << G4endl;
        }
        SetProcessSubType(fHadronAtRest);
        G4HadronicProcessStore::Instance()->RegisterExtraProcess(this);
    }

    MusrMuFormationAtRest::~MusrMuFormationAtRest() {
        G4HadronicProcessStore::Instance()->DeRegisterExtraProcess(this);
    }

    void MusrMuFormationAtRest::PreparePhysicsTable(const G4ParticleDefinition &p) {
        G4HadronicProcessStore::Instance()->RegisterParticleForExtraProcess(this, &p);
    }

    void MusrMuFormationAtRest::BuildPhysicsTable(const G4ParticleDefinition &p) {
        G4HadronicProcessStore::Instance()->PrintInfo(&p);
    }

// methods.............................................................................

    G4bool MusrMuFormationAtRest::IsApplicable(const G4ParticleDefinition &sParticle, const G4Track &aTrack) {
        std::cout << ">>>>>>>>>>>> APPLICABLE? " << aTrack.GetTrackID() << std::endl;
        return (&sParticle == G4MuonPlus::MuonPlus() && aTrack.GetTrackID() == 1);
    }


    G4VParticleChange *MusrMuFormationAtRest::AtRestDoIt(const G4Track &trackData, const G4Step &aStep) {
        bool primtrack = trackData.GetTrackID() == 1;
        if (!primtrack) {
            std::cout << ">>>>>>>>>>>> I RETURN WITH " << trackData.GetTrackID() << std::endl;
            return &fParticleChange;
        }

        std::cout << ">>>>>>>>>>>> IM STILL HERE " << trackData.GetTrackID() << std::endl;
        fParticleChange.Initialize(trackData);

        G4Track theNewTrack;

        p_name = aStep.GetTrack()->GetDefinition()->GetParticleName(); // particle name
        std::string logVolName = aStep.GetTrack()->GetVolume()->GetLogicalVolume()->GetName();


        G4double form = 0.5; //probability of forming NO muonium in given volume
        rnd = G4UniformRand();

        if (p_name == "mu+" && logVolName == "target" && rnd < form) {
            SetMuonium(&aStep);
            fParticleChange.SetNumberOfSecondaries(1);
            PrepareSecondary(trackData);
            fParticleChange.AddSecondary(aSecondary);
            fParticleChange.ProposeLocalEnergyDeposit(0.0);
            fParticleChange.ProposeTrackStatus(fStopAndKill);

        } else if (p_name == "mu+") {
            fParticleChange.ProposeTrackStatus(fSuspend);
        }


        ResetNumberOfInteractionLengthLeft();

        return &fParticleChange;
    }


    G4double
    MusrMuFormationAtRest::AtRestGetPhysicalInteractionLength(const G4Track &track, G4ForceCondition *condition) {
        // beggining of tracking
        ResetNumberOfInteractionLengthLeft();

        // condition is set to "Not Forced"
        *condition = NotForced;

        // get mean life time
        currentInteractionLength = GetMeanLifeTime(track, condition);

        if (currentInteractionLength < 0.0) {
            G4cout << "muonAtRestProcess::AtRestGetPhysicalInteractionLength ";
            G4cout << "[ " << GetProcessName() << "]" << G4endl;
            track.GetDynamicParticle()->DumpInfo();
            G4cout << " in Material  " << track.GetMaterial()->GetName() << G4endl;
            G4cout << "MeanLifeTime = " << currentInteractionLength / CLHEP::ns << "[ns]" << G4endl;
        }

        return theNumberOfInteractionLengthLeft * currentInteractionLength;

    }


    void MusrMuFormationAtRest::SetMuonium(const G4Step *aStep) {
        particleTable = G4ParticleTable::GetParticleTable();
        G4String p_new = "Mu";
        particle = particleTable->FindParticle(p_new);

        double T = 270;
        double dT = 70.;

        double pi = 3.14159265358979323846;

        double ran1 = G4UniformRand();
        double ran2 = G4UniformRand();


        double sin_theta = std::sqrt(ran1);
        double cos_theta = std::sqrt(1 - sin_theta * sin_theta);

        // random in plane angle
        double psi = ran2 * 2 * pi;

        // three vector direction components
        double a = sin_theta * std::cos(psi);
        double b = sin_theta * std::sin(psi);
        double c = cos_theta;


        double e0 = (T * 8.61738569e-5);
        double eSigma = (dT * 8.61738569e-5);

        double eKin = 0.;
        while (eKin <= 0) {
            eKin = G4RandGauss::shoot(e0, eSigma);
        }

        DP = new G4DynamicParticle(particle, G4ThreeVector(a, b, -c), eKin * CLHEP::eV);

        // IMPORTANT : COPY TRACK DATA TO GET THE SAME PARTICLE PROPERTIES!!!
        // Proper time: time since creation. Decay time is preassigned from this

        DP->SetProperTime(aStep->GetTrack()->GetDynamicParticle()->GetProperTime());
        DP->SetPolarization(aStep->GetTrack()->GetDynamicParticle()->GetPolarization().x(),
                            aStep->GetTrack()->GetDynamicParticle()->GetPolarization().y(),
                            aStep->GetTrack()->GetDynamicParticle()->GetPolarization().z());
        DP->SetPreAssignedDecayProperTime(aStep->GetTrack()->GetDynamicParticle()->GetPreAssignedDecayProperTime());

    }

    void MusrMuFormationAtRest::SetMuon(const G4Step *aStep) {
        particleTable = G4ParticleTable::GetParticleTable();
        particle = particleTable->FindParticle("mu+");

        DP = new G4DynamicParticle(particle, aStep->GetTrack()->GetDynamicParticle()->GetMomentumDirection(),
                                   aStep->GetTrack()->GetDynamicParticle()->GetKineticEnergy());
        DP->SetProperTime(aStep->GetTrack()->GetDynamicParticle()->GetProperTime());
        DP->SetPolarization(aStep->GetTrack()->GetDynamicParticle()->GetPolarization().x(),
                            aStep->GetTrack()->GetDynamicParticle()->GetPolarization().y(),
                            aStep->GetTrack()->GetDynamicParticle()->GetPolarization().z());
        DP->SetPreAssignedDecayProperTime(aStep->GetTrack()->GetDynamicParticle()->GetPreAssignedDecayProperTime());
    }


    void MusrMuFormationAtRest::PrepareSecondary(const G4Track &track) {
        if (p_name == "mu+") {
            aSecondary = new G4Track(DP, track.GetGlobalTime(), track.GetPosition());
        }
    }


}