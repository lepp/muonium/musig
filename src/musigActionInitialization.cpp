//
// Created by damian on 09.01.22.
//

#include "musigActionInitialization.h"

#include "musigPrimaryGeneratorAction.h"
#include "musigRunAction.h"
#include "musigEventAction.h"
#include "musigSteppingAction.h"


namespace MuSiG {


    ActionInitialization::ActionInitialization() = default;


    ActionInitialization::~ActionInitialization() = default;


    void ActionInitialization::Build() const {
        SetUserAction(new PrimaryGeneratorAction());
        SetUserAction(new RunAction());
        SetUserAction(new EventAction());
        SetUserAction(new SteppingAction());
    }


}
