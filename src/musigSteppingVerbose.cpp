#include "musigSteppingVerbose.h"

#include <G4SteppingManager.hh>
#include <G4UnitsTable.hh>
#include <G4SystemOfUnits.hh>
#include <G4RootAnalysisManager.hh>


namespace MuSiG {


// constructor

    SteppingVerbose::SteppingVerbose() = default;

// destructor

    SteppingVerbose::~SteppingVerbose() = default;


    void SteppingVerbose::StepInfo() {
        auto analMan = G4RootAnalysisManager::Instance();

        CopyState();

        G4int prec = G4cout.precision(3);


        if (verboseLevel >= 1) {
            if (verboseLevel >= 4) {
                VerboseTrack();
            }
            if (verboseLevel >= 3) {
                G4cout << G4endl;
                G4cout << std::setw(5) << "#Step#" << " " << std::setw(6) << "X" << "    " << std::setw(6) << "Y"
                       << "    " << std::setw(6) << "Z" << "    " << std::setw(9) << "KineE" << " " << std::setw(9)
                       << "dEStep" << " " << std::setw(10) << "StepLeng" << std::setw(10) << "TrakLeng" << std::setw(10)
                       << "Volume" << "  " << std::setw(10) << "Process" << G4endl;
            }

            G4cout << std::setw(5) << fTrack->GetCurrentStepNumber() << " " << std::setw(6)
                   << G4BestUnit(fTrack->GetPosition().x(), "Length") << std::setw(6)
                   << G4BestUnit(fTrack->GetPosition().y(), "Length") << std::setw(6)
                   << G4BestUnit(fTrack->GetPosition().z(), "Length") << std::setw(6)
                   << G4BestUnit(fTrack->GetKineticEnergy(), "Energy") << std::setw(6)
                   << G4BestUnit(fStep->GetTotalEnergyDeposit(), "Energy") << std::setw(6)
                   << G4BestUnit(fStep->GetStepLength(), "Length") << std::setw(6)
                   << G4BestUnit(fTrack->GetTrackLength(), "Length") << "  ";

            if (fTrack->GetNextVolume()) {
                G4cout << std::setw(10) << fTrack->GetVolume()->GetName();
            } else {
                G4cout << std::setw(10) << "OutOfWorld";
            }

            if (fStep->GetPostStepPoint()->GetProcessDefinedStep()) {
                G4cout << "  " << std::setw(10) << fStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName();
            } else {
                G4cout << "   UserLimit";
            }

            G4cout << G4endl;

            if (verboseLevel == 0) {

                G4int tN2ndariesTot = fN2ndariesAtRestDoIt + fN2ndariesAlongStepDoIt + fN2ndariesPostStepDoIt;


                if (tN2ndariesTot > 0) {
                    for (size_t lp1 = (fSecondary->size() - tN2ndariesTot); lp1 < fSecondary->size(); lp1++) {
                        const auto &fs = (*fSecondary)[lp1];

                        // Keep in sync with musigRunAction.cc!
                        analMan->FillNtupleDColumn(2, 0, (fs->GetGlobalTime() / ns)); // testTime
                        analMan->FillNtupleDColumn(2, 1, (fs->GetKineticEnergy() / keV)); // testEnergy
                        analMan->FillNtupleSColumn(2, 2, fs->GetDefinition()->GetParticleName()); // testName
                        analMan->FillNtupleSColumn(2, 3, fs->GetCreatorProcess()->GetProcessName()); // testProc
                        analMan->AddNtupleRow(2);

                        analMan->FillNtupleDColumn(3, 0, (fs->GetGlobalTime() / ns)); // x
                        analMan->FillNtupleDColumn(3, 1, (fs->GetPosition().y() / mm)); // y
                        analMan->FillNtupleDColumn(3, 2, (fs->GetPosition().z() / mm)); // z
                        analMan->AddNtupleRow(3);
                    }

                    G4cout << "    :-----------------------------" << "----------------------------------"
                           << "-- EndOf2ndaries Info ---------------" << G4endl;
                }
            }

        }
        G4cout.precision(prec);
    }

//--------------------------------------------------------------------------------

    void SteppingVerbose::TrackingStarted() {

        CopyState();
        G4int prec = G4cout.precision(3);
        if (verboseLevel > 0) {

            G4cout << std::setw(5) << "Step#" << " " << std::setw(6) << "X" << "    " << std::setw(6) << "Y" << "    "
                   << std::setw(6) << "Z" << "    " << std::setw(9) << "KineE" << " " << std::setw(9) << "dEStep" << " "
                   << std::setw(10) << "StepLeng" << std::setw(10) << "TrakLeng" << std::setw(10) << "Volume" << "  "
                   << std::setw(10) << "Process" << G4endl;

            G4cout << std::setw(5) << fTrack->GetCurrentStepNumber() << " " << std::setw(6)
                   << G4BestUnit(fTrack->GetPosition().x(), "Length") << std::setw(6)
                   << G4BestUnit(fTrack->GetPosition().y(), "Length") << std::setw(6)
                   << G4BestUnit(fTrack->GetPosition().z(), "Length") << std::setw(6)
                   << G4BestUnit(fTrack->GetKineticEnergy(), "Energy") << std::setw(6)
                   << G4BestUnit(fStep->GetTotalEnergyDeposit(), "Energy") << std::setw(6)
                   << G4BestUnit(fStep->GetStepLength(), "Length") << std::setw(6)
                   << G4BestUnit(fTrack->GetTrackLength(), "Length") << "  ";

            if (fTrack->GetNextVolume()) {
                G4cout << std::setw(10) << fTrack->GetVolume()->GetName();
            } else {
                G4cout << std::setw(10) << "OutOfWorld";
            }
            G4cout << "    initStep" << G4endl;
        }
        G4cout.precision(prec);
    }


}