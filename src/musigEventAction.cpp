#include "musigEventAction.h"

#include <G4Event.hh>
#include <G4EventManager.hh>
#include <G4Trajectory.hh>
#include <G4ios.hh>
#include <G4PhysicalConstants.hh>
#include <G4SystemOfUnits.hh>

#include "musigTrackerHit.h"


namespace MuSiG {


    EventAction::EventAction() = default;

    EventAction::~EventAction() = default;


// Beginning of event action. Free up the containers.

    void EventAction::BeginOfEventAction(const G4Event *) {}

// End of event action. We count the hits at the and of every event.

    void EventAction::EndOfEventAction(const G4Event *evt) {
        const auto event_id = evt->GetEventID();

        // periodic printing
        if (event_id < 100 || event_id % 10000 == 0) {
            G4cout << "---------------------------" << G4endl;
            G4cout << ">>> Event " << evt->GetEventID() << G4endl;
            G4cout << ">>> the number of primary vertices:  " << evt->GetNumberOfPrimaryVertex() << G4endl;

        }


    }


}