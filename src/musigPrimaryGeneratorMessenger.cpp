#include "musigPrimaryGeneratorMessenger.h"

#include <G4UIdirectory.hh>
#include <G4UIcmdWithADoubleAndUnit.hh>
#include <G4UIcmdWithADouble.hh>
#include <G4UIcmdWithAString.hh>
#include <G4UIcmdWith3Vector.hh>
#include <G4UIcmdWith3VectorAndUnit.hh>
#include <G4UIcmdWithALongInt.hh>

#include "musigPrimaryGeneratorAction.h"


namespace MuSiG {


    PrimaryGeneratorMessenger::PrimaryGeneratorMessenger(PrimaryGeneratorAction *Gun) : G4UImessenger(), fAction(Gun) {
        setGunDir = new G4UIdirectory("/gun/");
        setGunDir->SetGuidance("gun control");

        setSeedCmd = new G4UIcmdWithALongInt("/gun/seed", this);
        setSeedCmd->SetGuidance("PRNG seed");
        setSeedCmd->SetParameterName("mes_seed", false);

        setPrimaryParticleCmd = new G4UIcmdWithAString("/gun/particle", this);
        setPrimaryParticleCmd->SetGuidance("Set the primary particle (default is mu+).");
        setPrimaryParticleCmd->SetParameterName("mes_particleName", true);
        setPrimaryParticleCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

        setVertexCmd = new G4UIcmdWith3VectorAndUnit("/gun/vertex", this);
        setVertexCmd->SetGuidance(" Set x0, y0, z0 of the generated muons (with unit)");
        setVertexCmd->SetParameterName("mes_x0", "mes_y0", "mes_z0", true, true);
        setVertexCmd->SetDefaultUnit("mm");

        setVertexSigmaCmd = new G4UIcmdWith3VectorAndUnit("/gun/vertexsigma", this);
        setVertexSigmaCmd->SetGuidance(" Set xSigma, ySigma, ySigma of the generated muons (with unit)");
        setVertexSigmaCmd->SetParameterName("mes_xSigma", "mes_ySigma", "mes_zSigma", true, true);
        setVertexSigmaCmd->SetDefaultUnit("mm");

        setVertexRelativeRCmd = new G4UIcmdWithADoubleAndUnit("/gun/vertexrelativer", this);
        setVertexRelativeRCmd->SetGuidance(" Set maximum allowed radius of the beam relative to x0 and y0,");
        setVertexRelativeRCmd->SetGuidance("    i.e. relative to the centre of the beam (with unit)");
        setVertexRelativeRCmd->SetParameterName("mes_relativeRMaxAllowed", true);
        setVertexRelativeRCmd->SetDefaultUnit("mm");

        setStarttimeCmd = new G4UIcmdWithADoubleAndUnit("/gun/starttime", this);
        setStarttimeCmd->SetGuidance(" Set start time t of the generated particles (with unit)");
        setStarttimeCmd->SetParameterName("mes_t0", true);
        setStarttimeCmd->SetDefaultUnit("ns");

        setStarttimeSigmaCmd = new G4UIcmdWithADoubleAndUnit("/gun/starttimesigma", this);
        setStarttimeSigmaCmd->SetGuidance(" Set start time sigma tSigma of the generated particles (with unit)");
        setStarttimeSigmaCmd->SetParameterName("mes_tSigma", true);
        setStarttimeSigmaCmd->SetDefaultUnit("ns");

        setKEnergyCmd = new G4UIcmdWithADoubleAndUnit("/gun/energy", this);
        setKEnergyCmd->SetGuidance("Set kinetic energy of the generated particles (with unit)");
        setKEnergyCmd->SetParameterName("mes_E0", true);
        setKEnergyCmd->SetDefaultUnit("MeV");

        setTiltAngleCmd = new G4UIcmdWith3VectorAndUnit("/gun/tilt", this);
        setTiltAngleCmd->SetGuidance(" Set tilt angle of the generated muons (with unit, z component ignored)");
        setTiltAngleCmd->SetParameterName("mes_xangle", "mes_yangle", "dummy", true, true);
        setTiltAngleCmd->SetDefaultUnit("deg");

        setSigmaTiltAngleCmd = new G4UIcmdWith3VectorAndUnit("/gun/tiltsigma", this);
        setSigmaTiltAngleCmd->SetGuidance(" Set sigma of the tilt angle (with unit, z component ignored)");
        setSigmaTiltAngleCmd->SetParameterName("mes_xangleSigma", "mes_yangleSigma", "dummy", true, true);
        setSigmaTiltAngleCmd->SetDefaultUnit("deg");

        setPitchCmd = new G4UIcmdWithADoubleAndUnit("/gun/pitch", this);
        setPitchCmd->SetGuidance(" Set pitch angle of the generated muons (with unit)");
        setPitchCmd->SetParameterName("mes_pitch", true);
        setPitchCmd->SetDefaultUnit("deg");

        setMomentumCmd = new G4UIcmdWithADoubleAndUnit("/gun/momentum", this);
        setMomentumCmd->SetGuidance(" Set mean momentum of the generated particles (with unit)");
        setMomentumCmd->SetParameterName("mes_p0", true);
        setMomentumCmd->SetDefaultUnit("MeV");

        setMomentumSigmaCmd = new G4UIcmdWithADoubleAndUnit("/gun/momentumsigma", this);
        setMomentumSigmaCmd->SetGuidance(" Set sigma of the momentum of the generated particles (with unit)");
        setMomentumSigmaCmd->SetParameterName("mes_pSigma", true);
        setMomentumSigmaCmd->SetDefaultUnit("MeV");

        setDirectionCmd = new G4UIcmdWith3Vector("/gun/direction", this);
        setDirectionCmd->SetGuidance("Set initial mu beam direction as a vector (without units)");
        setDirectionCmd->SetGuidance("    The vector does not have to be normalised to 1");
        setDirectionCmd->SetParameterName("DirectionX", "DirectionY", "DirectionZ", true, true);

        setMuonPolarizCmd = new G4UIcmdWith3Vector("/gun/muonPolarizVector", this);
        setMuonPolarizCmd->SetGuidance("Set initial mu polarisation as a vector (without units)");
        setMuonPolarizCmd->SetGuidance("    The vector does not have to be normalised to 1");
        setMuonPolarizCmd->SetParameterName("mes_polarisX", "mes_polarisY", "mes_polarisZ", true, true);

        setMuonPolarizFractionCmd = new G4UIcmdWithADouble("/gun/muonPolarizFraction", this);
        setMuonPolarizFractionCmd->SetGuidance(" Set the fraction of the muon polarisation (in the range of -1 to 1),");
        setMuonPolarizFractionCmd->SetGuidance(
                " where fraction = (N_up_spin - N_down_spin) / (N_up_spin + N_down_spin)");
        setMuonPolarizFractionCmd->SetParameterName("mes_polarisFraction", true);

    }

    PrimaryGeneratorMessenger::~PrimaryGeneratorMessenger() {
        delete setGunDir;
        delete setSeedCmd;
        delete setPrimaryParticleCmd;
        delete setVertexCmd;
        delete setVertexSigmaCmd;
        delete setVertexRelativeRCmd;
        delete setStarttimeCmd;
        delete setStarttimeSigmaCmd;
        delete setKEnergyCmd;
        delete setMomentumCmd;
        delete setMomentumSigmaCmd;
        delete setTiltAngleCmd;
        delete setSigmaTiltAngleCmd;
        delete setPitchCmd;
        delete setDirectionCmd;
        delete setMuonPolarizCmd;
        delete setMuonPolarizFractionCmd;
    }

    void PrimaryGeneratorMessenger::SetNewValue(G4UIcommand *command, G4String newValue) {
        if (command == setSeedCmd) {
            fAction->SetSeed(G4UIcmdWithALongInt::GetNewLongIntValue(newValue));
        } else if (command == setPrimaryParticleCmd) {
            fAction->SetPrimaryParticle(newValue);
        } else if (command == setVertexCmd) {
            fAction->SetVertex(G4UIcmdWith3VectorAndUnit::GetNew3VectorValue(newValue));
        } else if (command == setVertexSigmaCmd) {
            fAction->SetVertexSigma(G4UIcmdWith3VectorAndUnit::GetNew3VectorValue(newValue));
        } else if (command == setStarttimeCmd) {
            fAction->SetStartTime(G4UIcmdWithADoubleAndUnit::GetNewDoubleValue(newValue));
        } else if (command == setStarttimeSigmaCmd) {
            fAction->SetStartTimeSigma(G4UIcmdWithADoubleAndUnit::GetNewDoubleValue(newValue));
        } else if (command == setVertexRelativeRCmd) {
            fAction->SetVertexRelativeR(G4UIcmdWithADoubleAndUnit::GetNewDoubleValue(newValue));
        } else if (command == setKEnergyCmd) {
            fAction->SetKEnergy(G4UIcmdWithADoubleAndUnit::GetNewDoubleValue(newValue));
        } else if (command == setMomentumCmd) {
            fAction->SetMomentum(G4UIcmdWithADoubleAndUnit::GetNewDoubleValue(newValue));
        } else if (command == setMomentumSigmaCmd) {
            fAction->SetMomentumSigma(G4UIcmdWithADoubleAndUnit::GetNewDoubleValue(newValue));
        } else if (command == setTiltAngleCmd) {
            fAction->SetTilt(G4UIcmdWith3VectorAndUnit::GetNew3VectorValue(newValue));
        } else if (command == setSigmaTiltAngleCmd) {
            fAction->SetSigmaTilt(G4UIcmdWith3VectorAndUnit::GetNew3VectorValue(newValue));
        } else if (command == setPitchCmd) {
            fAction->SetPitch(G4UIcmdWithADoubleAndUnit::GetNewDoubleValue(newValue));
        } else if (command == setDirectionCmd) {
            fAction->SetBeamDirection(G4UIcmdWith3Vector::GetNew3VectorValue(newValue));
        } else if (command == setMuonPolarizCmd) {
            fAction->SetInitialMuonPolariz(G4UIcmdWith3Vector::GetNew3VectorValue(newValue));
        } else if (command == setMuonPolarizFractionCmd) {
            fAction->SetInitialPolarizFraction(G4UIcmdWithADouble::GetNewDoubleValue(newValue));
        }

    }


}