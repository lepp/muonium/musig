//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file electromagnetic/TestEm7/src/PhysListEmStandardNR.cc
/// \brief Implementation of the PhysListEmStandardNR class
//
// $Id: PhysListEmStandardNR.cc 100284 2016-10-17 08:41:43Z gcosmo $
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "musigPhysListEmStandardNR.h"

#include <G4SystemOfUnits.hh>
#include <G4ParticleDefinition.hh>
#include <G4LossTableManager.hh>
#include <G4ComptonScattering.hh>
#include <G4GammaConversion.hh>
#include <G4PhotoElectricEffect.hh>
#include <G4RayleighScattering.hh>
#include <G4PEEffectFluoModel.hh>
#include <G4KleinNishinaModel.hh>
#include <G4PenelopeGammaConversionModel.hh>
#include <G4LivermorePhotoElectricModel.hh>
#include <G4eMultipleScattering.hh>
#include <G4MuMultipleScattering.hh>
#include <G4hMultipleScattering.hh>
#include <G4eCoulombScatteringModel.hh>
#include <G4UrbanMscModel.hh>
#include <G4eIonisation.hh>
#include <G4eBremsstrahlung.hh>
#include <G4eplusAnnihilation.hh>
#include <G4UAtomicDeexcitation.hh>
#include <G4MuIonisation.hh>
#include <G4MuBremsstrahlung.hh>
#include <G4MuPairProduction.hh>
#include <G4hIonisation.hh>
#include <G4ionIonisation.hh>
#include <G4IonParametrisedLossModel.hh>
#include <G4PhysicsListHelper.hh>
#include <G4BuilderType.hh>
#include <G4Decay.hh>
#include <G4ProcessManager.hh>

#include "G4ScreenedNuclearRecoil.hh"
#include "musigMusrMuScatter.h"
#include "musigMusrMuFormation.h"


namespace MuSiG {


    PhysListEmStandardNR::PhysListEmStandardNR(const G4String &name) : G4VPhysicsConstructor(name) {
        G4LossTableManager::Instance();
        SetPhysicsType(bElectromagnetic);
    }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

    PhysListEmStandardNR::~PhysListEmStandardNR() = default;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

    void PhysListEmStandardNR::ConstructProcess() {

        std::cout << " >>>>>>>>>>>>>>  Constructing EM Standard NR" << std::endl;
        G4PhysicsListHelper *ph = G4PhysicsListHelper::GetPhysicsListHelper();

        // muon & hadron bremsstrahlung and pair production
        new G4MuBremsstrahlung();
        new G4MuPairProduction();

        auto nucr = new G4ScreenedNuclearRecoil();
        G4double energyLimit = 100. * MeV;
        nucr->SetMaxEnergyForScattering(energyLimit);
        auto csm = new G4eCoulombScatteringModel();
        csm->SetActivationLowEnergyLimit(energyLimit);

        auto particleIterator = GetParticleIterator();
        particleIterator->reset();
        while ((*particleIterator)()) {
            G4ParticleDefinition *particle = particleIterator->value();
            G4String particleName = particle->GetParticleName();
            G4ProcessManager *pManager = particle->GetProcessManager();

            if (particleName == "gamma") {

                // Compton scattering
                auto cs = new G4ComptonScattering;
                cs->SetEmModel(new G4KleinNishinaModel(), 1);
                ph->RegisterProcess(cs, particle);

                // Photoelectric
                auto pe = new G4PhotoElectricEffect();
                G4VEmModel *theLivermorePEModel = new G4LivermorePhotoElectricModel();
                theLivermorePEModel->SetHighEnergyLimit(10 * GeV);
                pe->SetEmModel(theLivermorePEModel, 1);
                ph->RegisterProcess(pe, particle);

                // Gamma conversion
                auto gc = new G4GammaConversion();
                G4VEmModel *thePenelopeGCModel = new G4PenelopeGammaConversionModel();
                thePenelopeGCModel->SetHighEnergyLimit(1 * GeV);
                gc->SetEmModel(thePenelopeGCModel, 1);
                ph->RegisterProcess(gc, particle);

                // Rayleigh scattering
                ph->RegisterProcess(new G4RayleighScattering(), particle);

            } else if (particleName == "e-") {

                // ionisation
                auto eIoni = new G4eIonisation();
                eIoni->SetStepFunction(0.2, 100 * um);

                // bremsstrahlung
                auto eBrem = new G4eBremsstrahlung();

                ph->RegisterProcess(new G4eMultipleScattering(), particle);
                ph->RegisterProcess(eIoni, particle);
                ph->RegisterProcess(eBrem, particle);

            } else if (particleName == "e+") {
                // ionisation
                auto eIoni = new G4eIonisation();
                eIoni->SetStepFunction(0.2, 100 * um);

                // bremsstrahlung
                auto eBrem = new G4eBremsstrahlung();

                ph->RegisterProcess(new G4eMultipleScattering(), particle);
                ph->RegisterProcess(eIoni, particle);
                ph->RegisterProcess(eBrem, particle);

                // annihilation at rest and in flight
                ph->RegisterProcess(new G4eplusAnnihilation(), particle);

            } else if (particleName == "mu+" || particleName == "mu-") {

                if (particleName == "mu+") {
                    pManager->AddProcess(new MusrMuFormation(), -1, -1, 1);
                }

                ph->RegisterProcess(new G4MuMultipleScattering(), particle);
                auto muIoni = new G4MuIonisation();
                muIoni->SetStepFunction(0.1, 50 * um);
                ph->RegisterProcess(muIoni, particle);
                ph->RegisterProcess(new G4MuBremsstrahlung(), particle);
                ph->RegisterProcess(new G4MuPairProduction(), particle);

            } else if (particleName == "Mu") {


                G4VProcess *aMuScatt = new MusrMuScatter();
                pManager->AddProcess(aMuScatt);
                pManager->SetProcessOrdering(aMuScatt, idxPostStep, 1);
                // ph->RegisterProcess(aMuScatt,particle);

                particle->GetProcessManager()->SetProcessOrdering(aMuScatt, idxPostStep, 1);

                auto theDecayProcess = new G4Decay();
                //musrDecayWithSpin* theDecayProcess = new musrDecayWithSpin();
                pManager->AddProcess(theDecayProcess);
                pManager->SetProcessOrderingToLast(theDecayProcess, idxAtRest);
                pManager->SetProcessOrdering(theDecayProcess, idxPostStep);


            } else if (particleName == "alpha" || particleName == "He3") {

                auto msc = new G4hMultipleScattering();
                auto model = new G4UrbanMscModel();
                model->SetActivationLowEnergyLimit(energyLimit);
                msc->SetEmModel(model, 1);
                ph->RegisterProcess(msc, particle);

                auto ionIoni = new G4ionIonisation();
                ionIoni->SetStepFunction(0.1, 10 * um);
                ph->RegisterProcess(ionIoni, particle);

                ph->RegisterProcess(nucr, particle);

            } else if (particleName == "GenericIon") {

                auto msc = new G4hMultipleScattering();
                auto model = new G4UrbanMscModel();
                model->SetActivationLowEnergyLimit(energyLimit);
                msc->SetEmModel(model, 1);
                ph->RegisterProcess(msc, particle);

                auto ionIoni = new G4ionIonisation();
                ionIoni->SetEmModel(new G4IonParametrisedLossModel());
                ionIoni->SetStepFunction(0.1, 1 * um);
                ph->RegisterProcess(ionIoni, particle);

                ph->RegisterProcess(nucr, particle);

            } else if (particleName == "proton" || particleName == "deuteron" || particleName == "triton") {

                auto msc = new G4hMultipleScattering();
                auto model = new G4UrbanMscModel();
                model->SetActivationLowEnergyLimit(energyLimit);
                msc->SetEmModel(model, 1);
                ph->RegisterProcess(msc, particle);

                auto hIoni = new G4hIonisation();
                hIoni->SetStepFunction(0.05, 1 * um);
                ph->RegisterProcess(hIoni, particle);

                ph->RegisterProcess(nucr, particle);


            } else if ((!particle->IsShortLived()) && (particle->GetPDGCharge() != 0.0) &&
                       (particle->GetParticleName() != "chargedgeantino")) {
                //all others charged particles except geantino

                ph->RegisterProcess(new G4hMultipleScattering(), particle);
                ph->RegisterProcess(new G4hIonisation(), particle);
            }
        }

        /* TODO: This no longer exists in Geant 4.11. Do we really need it?
        // Em options
        //
        // Main options and setting parameters are shown here.
        // Several of them have default values.
        //
        G4EmProcessOptions emOptions;

        //physics tables
        //
        emOptions.SetMinEnergy(10 * eV);
        emOptions.SetMaxEnergy(10 * TeV);
        emOptions.SetDEDXBinning(12 * 20);
        emOptions.SetLambdaBinning(12 * 20);

        // scattering
        emOptions.SetPolarAngleLimit(0.0);
         */

        // Deexcitation
        G4VAtomDeexcitation *de = new G4UAtomicDeexcitation();
        G4LossTableManager::Instance()->SetAtomDeexcitation(de);
        de->SetFluo(true);
    }


}