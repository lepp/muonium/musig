//
///  from \file electromagnetic/TestEm7/src/StepMaxMessenger.cc
/// \brief Implementation of the StepMaxMessenger class
//
// $Id: StepMaxMessenger.cc 67268 2013-02-13 11:38:40Z ihrivnac $


#include "musigStepMaxMessenger.h"

#include <G4UIcmdWithADoubleAndUnit.hh>

#include "musigStepMax.h"


namespace MuSiG {


    StepMaxMessenger::StepMaxMessenger(StepMax *stepM) : G4UImessenger(), fStepMax(stepM) {
        controlDir = new G4UIdirectory("/control/");
        controlDir->SetGuidance("step and other process control");


        fStepMaxCmd = new G4UIcmdWithADoubleAndUnit("/control/stepMax", this);
        fStepMaxCmd->SetGuidance("Set max allowed step length");
        fStepMaxCmd->SetParameterName("mxStep", false);
        fStepMaxCmd->SetRange("mxStep>0.");
        fStepMaxCmd->SetUnitCategory("Length");
    }

    StepMaxMessenger::~StepMaxMessenger() {
        delete controlDir;
        delete fStepMaxCmd;
    }


    void StepMaxMessenger::SetNewValue(G4UIcommand *command, G4String newValue) {
        if (command == fStepMaxCmd) {
            fStepMax->SetMaxStep(G4UIcmdWithADoubleAndUnit::GetNewDoubleValue(newValue));
        }
    }


}