#include "musigTrackerSD.h"

#include <G4HCofThisEvent.hh>
#include <G4Step.hh>
#include <G4ThreeVector.hh>
#include <G4SDManager.hh>
#include <G4ios.hh>
#include <G4SystemOfUnits.hh>
#include <G4EventManager.hh>
#include <G4Event.hh>
#include <G4RootAnalysisManager.hh>

#include <cstring>
#include <vector>
#include <map>
#include <algorithm>


namespace MuSiG {


//------------------------constructor---------------------------------------

    TrackerSD::TrackerSD(const G4String &name) : G4VSensitiveDetector(name) {
        collectionName.insert("trackerCollection");
    }

//-----------------------destructor--------------------------------------------

    TrackerSD::~TrackerSD() = default;

//----------------------------------------------------------------------------

    void TrackerSD::Initialize(G4HCofThisEvent *HCE) {
        trackerCollection = new TrackerHitsCollection(SensitiveDetectorName, collectionName[0]);
        static G4int HCID = -1;
        if (HCID < 0) {
            HCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
        }
        HCE->AddHitsCollection(HCID, trackerCollection);
    }

//----------------------------------------------------------------------------

    G4bool TrackerSD::ProcessHits(G4Step *aStep, G4TouchableHistory *) {
        const auto edep = aStep->GetTotalEnergyDeposit() / keV;
        if (edep == 0.) {
            return false;
        }

        const auto evtNr = G4EventManager::GetEventManager()->GetConstCurrentEvent()->GetEventID();

        auto hit = new TrackerHit();

        hit->SetEdep(edep);

        hit->SetParticleName(aStep->GetTrack()->GetDefinition()->GetParticleName());
        hit->SetParticleVertex(aStep->GetTrack()->GetVertexPosition());
        hit->SetPos(aStep->GetPostStepPoint()->GetPosition());
        hit->SetTime(aStep->GetPreStepPoint()->GetGlobalTime() / ns);
        hit->SetEventNb(evtNr);
        hit->SetTrackID(aStep->GetTrack()->GetTrackID());
        hit->SetDetectorName(aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName());
        hit->SetChamberNb(aStep->GetPreStepPoint()->GetTouchableHandle()->GetCopyNumber());

        trackerCollection->insert(hit);

        return true;
    }

//----------------------------------------------------------------------------

    void TrackerSD::EndOfEvent(G4HCofThisEvent *) {
        auto analMan = G4RootAnalysisManager::Instance();

        const auto NbHits = trackerCollection->entries();

        if (verboseLevel > 0) {
            G4cout << "\n-------->Hits Collection: in this event there are " << NbHits
                   << " hits in the tracker chambers: " << G4endl;
            for (std::size_t i = 0; i < NbHits; ++i) {
                (*trackerCollection)[i]->Print();
            }
        }

        std::map<std::string, std::map<int, std::vector<double>>> MinMap;
        std::map<std::string, std::map<int, double[2]>> EnergyTimeMap;

        for (std::size_t i = 0; i < NbHits; ++i) {
            const auto &hit = (*trackerCollection)[i];

            // Keep in sync with musigRunAction.cpp!
            analMan->FillNtupleIColumn(0, 0, hit->GetEventNb());
            analMan->FillNtupleSColumn(0, 1, hit->GetParticleName());
            analMan->FillNtupleIColumn(0, 2, hit->GetTrackID());
            analMan->FillNtupleSColumn(0, 3, hit->GetDetectorName());
            analMan->FillNtupleIColumn(0, 4, hit->GetChamberNb());
            analMan->FillNtupleFColumn(0, 5, hit->GetEdep());
            analMan->FillNtupleFColumn(0, 6, hit->GetPos().x());
            analMan->FillNtupleFColumn(0, 7, hit->GetPos().y());
            analMan->FillNtupleFColumn(0, 8, hit->GetPos().z());
            analMan->FillNtupleFColumn(0, 9, (hit->GetTime() / ns));
            analMan->FillNtupleFColumn(0, 10, hit->GetParticleVertex().x());
            analMan->FillNtupleFColumn(0, 11, hit->GetParticleVertex().y());
            analMan->FillNtupleFColumn(0, 12, hit->GetParticleVertex().z());
            analMan->AddNtupleRow(0);

            ///// below:  old code for integrated data /////

            const auto &IDetname = hit->GetDetectorName();
            const auto IChamberNb = hit->GetChamberNb();

            EnergyTimeMap[IDetname][IChamberNb][0] += hit->GetEdep();

            //-- We choose the moment when the first particle left energy in the segment as the Hit Time of that segment:
            auto &&minVector = MinMap[IDetname][IChamberNb];
            minVector.push_back(hit->GetTime());
            EnergyTimeMap[IDetname][IChamberNb][1] = *std::min_element(minVector.cbegin(), minVector.cend());
        }

        const auto eventId = G4EventManager::GetEventManager()->GetConstCurrentEvent()->GetEventID();

        for (const auto &detector: EnergyTimeMap) {
            for (const auto &chamber: detector.second) {
                // Keep in sync with musigRunAction.cpp!
                analMan->FillNtupleFColumn(1, 0, chamber.second[0]); // Iedeposit
                analMan->FillNtupleFColumn(1, 1, chamber.second[1]); // Itime
                analMan->FillNtupleIColumn(1, 2, eventId);
                analMan->FillNtupleIColumn(1, 3, chamber.first); // IchamberNb
                analMan->FillNtupleSColumn(1, 4, detector.first); // Idetname
                analMan->AddNtupleRow(1);
            }
        }

        // No, see http://en.cppreference.com/w/cpp/container/vector/~vector
        //MinMap.clear();  // at the end of the event one has to empty the minmap!!

    }


}
