#include "musigSteppingAction.h"

#include <G4SteppingManager.hh>
#include <G4SystemOfUnits.hh>
#include <G4EventManager.hh>
#include <G4Event.hh>
#include <G4RootAnalysisManager.hh>


namespace MuSiG {


    SteppingAction::SteppingAction() = default;

    SteppingAction::~SteppingAction() = default;

    void SteppingAction::UserSteppingAction(const G4Step *step) {
        auto analMan = G4RootAnalysisManager::Instance();

        const auto &SPartName = step->GetTrack()->GetDefinition()->GetParticleName();
        const auto &SPhysVolume = step->GetPreStepPoint()->GetPhysicalVolume()->GetName();
        const auto SGlobalTime = step->GetPreStepPoint()->GetGlobalTime() / ns;
        const auto SKinE = step->GetPreStepPoint()->GetKineticEnergy();
        const auto SStepNb = step->GetTrack()->GetCurrentStepNumber();
        const auto SParentID = step->GetTrack()->GetParentID();


//---------------------------------------------
//           Stopping distribution in He 
//---------------------------------------------


        if ((SParentID == 0) && ((step->GetTrack()->GetTrackStatus() == fStopButAlive) ||
                                 (step->GetTrack()->GetTrackStatus() == fStopAndKill) ||
                                 (step->GetTrack()->GetTrackStatus() == fSuspend))) {

            // Keep in sync with musigRunAction.cc!
            analMan->FillNtupleDColumn(3, 0, SGlobalTime); // x
            analMan->FillNtupleDColumn(3, 1, (step->GetTrack()->GetPosition().y() / mm)); // y
            analMan->FillNtupleDColumn(3, 2, (step->GetTrack()->GetPosition().z() / mm)); // z
            analMan->FillNtupleIColumn(3, 3, 1); // stopHP
            analMan->FillNtupleIColumn(3, 4, (SPhysVolume == "det1")); // stopP
            analMan->FillNtupleIColumn(3, 5, (SPhysVolume == "ti_foil")); // stopF
            analMan->FillNtupleIColumn(3, 6, (SPhysVolume == "chamber")); // stopE
            analMan->AddNtupleRow(3);
        }


//---------------------------------------------------------
//            COUNTING THE SECONDARIES	(SecondaryHisto.cc)
//---------------------------------------------------------

        //Secondary particles, written out in TestHisto


        if ((SStepNb == 1) && (SParentID == 1)) {
            // Keep in sync with musigRunAction.cc!
            analMan->FillNtupleDColumn(2, 0, SGlobalTime); // testTime
            analMan->FillNtupleDColumn(2, 1, SKinE); // testEnergy
            analMan->FillNtupleSColumn(2, 2, SPartName); // testName
            analMan->FillNtupleSColumn(2, 3, step->GetTrack()->GetCreatorProcess()->GetProcessName()); // testProc
            analMan->FillNtupleIColumn(2, 4, (SPartName == "gamma")); // counter1
            analMan->AddNtupleRow(2);
        }


//--------------------------------------------------
// there are late hits (one or two out of half million) occuring from some radioactive decay.
// we abort these events.
//---------------------------------------------------

        if (SGlobalTime > 1000 * us) {
            step->GetTrack()->SetTrackStatus(fKillTrackAndSecondaries);
        }


    }


}
