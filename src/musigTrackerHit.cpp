#include "musigTrackerHit.h"

#include <G4UnitsTable.hh>
#include <G4VVisManager.hh>
#include <G4Circle.hh>
#include <G4Colour.hh>
#include <G4VisAttributes.hh>


namespace MuSiG {


//---------------constructor I (default)-----------------------------

    TrackerHit::TrackerHit() = default;

//--------------- destructor-----------------------------------------

    TrackerHit::~TrackerHit() = default;

//----------------costructor II--------------------------------------

    TrackerHit::TrackerHit(const TrackerHit &right) : G4VHit() {
        eventNb = right.eventNb;
        trackID = right.trackID;
        detectorName = right.detectorName;
        chamberNb = right.chamberNb;
        edep = right.edep;
        pos = right.pos;
        time = right.time;
        particleName = right.particleName;
        particleVertex = right.particleVertex;
    }

//--------------------------------------------------------------------

    TrackerHit &TrackerHit::operator=(const TrackerHit &right) {
        // Gracefully handle self-assignment
        if (this == &right) {
            return *this;
        }

        eventNb = right.eventNb;
        trackID = right.trackID;
        detectorName = right.detectorName;
        chamberNb = right.chamberNb;
        edep = right.edep;
        pos = right.pos;
        time = right.time;
        particleName = right.particleName;
        particleVertex = right.particleVertex;

        return *this;
    }

//---------------------------------------------------------------------

    G4int TrackerHit::operator==(const TrackerHit &right) const {
        return (this == &right) ? 1 : 0;
    }


    void *TrackerHit::operator new(std::size_t) {
        return static_cast<void *>(TrackerHitAllocator().MallocSingle());
    }


    void TrackerHit::operator delete(void *aHit) {
        TrackerHitAllocator().FreeSingle(static_cast<TrackerHit *>(aHit));
    }


    void TrackerHit::Add(G4double de) {
        edep += de;
    }


// to vis manager: drawing the hits

    void TrackerHit::Draw() {
        auto pVVisManager = G4VVisManager::GetConcreteInstance();
        if (pVVisManager) {
            G4Circle circle(pos);
            circle.SetScreenSize(0.2);
            circle.SetFillStyle(G4Circle::filled);
            G4Colour colour(1., 0., 0.);                   //set color to red
            G4VisAttributes attribs(colour);
            circle.SetVisAttributes(attribs);
            pVVisManager->Draw(circle);
        }
    }

//-----------------------------------------------------------------------


    void TrackerHit::Print() {
        G4cout << " ------------------------------------- " << G4endl;
        G4cout << "Particle Name: " << particleName << " Hit time: " << time << G4endl;
        G4cout << "  trackID: " << trackID << "  det.Name  " << detectorName << "  chamberNb: " << chamberNb
               << "  energy deposit: " << G4BestUnit(edep, "Energy") << "  position: " << G4BestUnit(pos, "Length")
               << G4endl;

    }


}