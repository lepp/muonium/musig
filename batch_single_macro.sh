#!/bin/bash


# Exit on error
set -eEuo pipefail


if (( ${#} < 5 )); then
  echo "Usage: $(basename "${BASH_SOURCE[0]}") MUSIGEXE OUTPUTDIR MACRO NEVENTS NJOBS"
  exit 1
fi


run() {
  musig_exe="${1}"
  output_dir="${2}"
  macro="${3}"
  nevents=${4}
  job_number=$(( ${5} - 1 )) # Start from 0
  seed=$(( 1 + ${job_number} * ${nevents} )) # Start from 1, 0 crashes
  output_file="${output_dir}/$(basename "${macro}" ".mac")_${job_number}"
  "${musig_exe}" -s ${seed} -n ${nevents} -o "${output_file}" "${macro}" > "${output_file}.log" 2>&1
  ret=${?}
  if (( ${ret} )); then
    echo "FAILURE"
  else
    echo "SUCCESS"
  fi
  return ${ret}
}
export -f run


musig_exe="${1}"
output_dir="${2}"
macro="${3}"
nevents=${4}
njobs=${5}
events_per_job=$(( ${nevents} / ${njobs} ))
parallel --bar --line-buffer --tag run "${musig_exe}" "${output_dir}" "${macro}" ${events_per_job} ::: $(seq ${njobs}) && ret=0 || ret=${?}
echo
if (( ${ret} < 0 || ${ret} > 101 )); then
  echo "ERROR: GNU parallel failed with status ${ret}!"
elif (( ${ret} == 101 )); then
  echo "WARNING: More than 100 jobs failed!"
elif (( ${ret} )); then
  echo "WARNING: ${ret} jobs failed!"
else
  echo "All jobs succeeded!"
fi
